package doubblepointer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lvhaibao
 * @description 剑指Offer 两数之和
 * 输入一个递增排序的数组和一个数字S，在数组中查找两个数，使得他们的和正好是S，如果有多对数字的和等于S，输出两个数的乘积最小的。
 * <p>
 * 输入：
 * [1,2,4,7,11,15],15
 * 返回值：[4,11]
 * @date 2021/6/21 23:30
 */
public class Test1 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        int s = 21;
        ArrayList<Integer> list = findNumbersWithSum(arr, s);
        System.out.println(list.toArray());
    }

    public static ArrayList<Integer> findNumbersWithSum(int[] array, int sum) {
        ArrayList<Integer> list = new ArrayList<>();
        if (array.length < 2) {
            return list;
        }
        int l = 0;
        int r = array.length - 1;
        int mul = Integer.MAX_VALUE;
        while (l <= r) {

            // 如果大于
            if ((array[l] + array[r]) > sum) {
                r--;
            }
            // 如果小于
            else if (array[l] + array[r] < sum) {
                l++;
            } else {
                int value = array[l] * array[r];
                // 判断乘积,如果小于,替换
                if (value < mul) {
                    mul = value;
                    list.clear();
                    list.add(array[l]);
                    list.add(array[r]);
                }
                // 双指针都变
                l++;
                r--;
            }
        }
        return list;
    }
}
