package doubblepointer;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

/**
 * @author lvhaibao
 * @description 三数之和
 * 给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。
 * 注意：答案中不可以包含重复的三元组。
 *
 * 给定数组 nums = [-1, 0, 1, 2, -1, -4]，
 * 满足要求的三元组集合为：
 * [
 * [-1, 0, 1],
 * [-1, -1, 2]
 * ]
 *
 * @date 2021/6/23 23:38
 */
public class Test3 {


    public static void main(String[] args) {


        /**
         * 输入：nums = [-1,0,1,2,-1,-4]
         * 输出：[[-1,-1,2],[-1,0,1]]
         */
        int[] nums = {-1,0,1,2,-1,-4};
        threeSum(nums);



    }

    public static List<List<Integer>> threeSum(int[] nums) {

        List<List<Integer>> alllist = new ArrayList<List<Integer>>();

        if(nums.length < 3) {
            return alllist;
        }
        Arrays.sort(nums);


        for(int i = 0; i<=nums.length-3;i++){
            if(i>0 && nums[i-1] == nums[i]){
                continue;
            }
            int target = nums[i];
            int left = i+1;
            int right = nums.length-1;
            while (left<right){

                int sum = nums[left]+ nums[right];

                // 如果等于
                if(sum == target){
                    List<Integer> list = new ArrayList<>();
                    list.add(target);
                    list.add(nums[left]);
                    list.add(nums[right]);
                    alllist.add(list);

                    // 可能有重复数字
                    while (left<right && nums[left] == nums[left+1]) {
                        left++;
                    }
                    left++;
                    while (left<right && nums[right] == nums[right-1]) {
                        right++;
                    }
                    right--;

                }

                // 如果大于，右指针移动，需要考虑重复数字
                else if(sum > target) {
                    while(left<right && nums[right]==nums[right-1]) {
                        right--;
                    }
                    right--;
                }
                // 如果小于，左指针移动，需要考虑重复数字
                else {
                    while(left<right && nums[left]==nums[left+1]) {
                        left++;
                    }
                    left++;
                }

            }
            return alllist;
        }
        return null;
    }
}
