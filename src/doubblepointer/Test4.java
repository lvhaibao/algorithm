package doubblepointer;

import java.util.Arrays;

/**
 * @author lvhaibao
 * @description 三数之和最接近
 * 给定一个包括 n 个整数的数组 nums 和 一个目标值 target。找出 nums 中的三个整数，使得它们的和与 target 最接近。返回这三个数的和。假定每组输入只存在唯一答案。
 *
 *输入：nums = [-1,2,1,-4], target = 1
 * 输出：2
 * 解释：与 target 最接近的和是 2 (-1 + 2 + 1 = 2) 。
 * @date 2021/6/24 22:52
 */
public class Test4 {

    public static void main(String[] args) {

        int[] arr = {-1,2,1,-4};
        int target= 1;


    }


    public static int threeSumClosest(int[] nums, int target) {

        Arrays.sort(nums);
        int res = 0;
        int min = Integer.MAX_VALUE;

        for(int i = 0; i<=nums.length-3;i++){

            int left = i+1;
            int right = nums.length-1;

            while (left<right){
                int sum = nums[i]+nums[left]+nums[right];
                int cha = Math.abs(sum-target);
                if(cha<min){
                    min = cha;
                    res = sum;
                }
                if(sum == target){
                    return target;
                }
                else if(sum>target){
                    right--;
                }else {
                    left++;
                }
            }
        }
        return res;
    }

}
