package doubblepointer;

/**
 * @author lvhaibao
 * @description
 * 输入：[1,8,6,2,5,4,8,3,7]
 * 输出：49
 * @date 2021/6/24 23:08
 */
public class Test5 {



    public int maxArea(int[] height) {

        int max = 0;

        int left = 0;
        int right = height.length-1;

        while (left<right){

            int w = Math.min(height[left], height[right]);
            int l = right-left;
            int mul = w*l;
            if(mul>max){
                max = mul;
            }

            // 如何移动左右指针？我们只有移动值较小的一个指针才会有可能使容积变大。
            // 因为我们计算容量取得是高度最小的那个作为容器的高度，也就是选择短板作为容器高度。
            // 所以移动左右两个指针值最小的那一个
            if(height[left] > height[right]){
                right--;
            }
            else if(height[left] < height[right]){
                left++;
            }else {
                left++;
                right--;
            }
        }
        return max;
    }







    public static void main(String[] args) {





    }
}
