package doubblepointer;

/**
 * @author lvhaibao
 * @description
 * 输入：nums = [2,0,2,1,1,0]
 * 输出：[0,0,1,1,2,2]
 * @date 2021/6/24 23:52
 */
public class Test6 {


    public static void main(String[] args) {

    }

    public void sortColors(int[] nums) {

        int p0 = 0;         //指向0的位置
        int p2 = nums.length - 1;   //指向2的位置
        for(int i = 0; i <= p2; i++) {
            while(i <= p2 && nums[i] == 2) {   //如果nums[i]==2，就移到最后的位置上去，因为不能确定nums[p2]的位置是不是2，所以要用while循环，保证最后都是2
                int t = nums[i];
                nums[i] = nums[p2];
                nums[p2] = t;
                p2--;
            }

            if(nums[i] == 0) {  //如果是0，就和前面标记的p0这个位置交换
                int t = nums[i];
                nums[i] = nums[p0];
                nums[p0] = t;
                p0++;
            }
        }


    }
}
