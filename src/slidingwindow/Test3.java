package slidingwindow;

/**
 * @author lvhaibao
 * @description
 * 题目描述：
 *
 * 最多带有K个不同字符的最长子字符串，子字符串不能含有重复字符
 *
 * 输入：5 ，“leetcode” 输出：5—“tcode”
 *
 * lete
 *
 * lee
 *
 * @date 2021/7/6 22:11
 */
public class Test3 {

    public static void main(String[] args) {

        System.out.println(KSubstring("leetcode",5));

    }



    public static int KSubstring(String s, int k) {

        String sub = "";
        int maxLen = 0;

        for(int i = 0;i<s.length();i++){

            while (sub.contains(Character.toString(s.charAt(i)))){
                // 一个一个地删除
                sub = sub.substring(1);
            }
            sub += Character.toString(s.charAt(i));

            if(sub.length() <= k) {
                maxLen = Math.max(maxLen, sub.length());
            }

        }

        return maxLen;



    }
}
