package slidingwindow;

/**
 * @author lvhaibao
 * @description
 * 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如果不存在，则返回 -1。
 *
 * 输入: haystack = “hello”, needle = “ll”
 * 输出: 2
 * @date 2021/7/6 23:09
 */
public class Test4 {

    public static void main(String[] args) {

    }


    // 思路：先定左指针，找到第一个匹配的地方，然后遍历要匹配的字符串
    // 当匹配完成之后，如果等于匹配字段的长度，代表成功
    public static int strStr(String haystack, String needle) {

        if(haystack.length() < needle.length()){
            return -1;
        }

        if(haystack.length() == 0 || needle.length() == 0){
            return 0;
        }

        for(int i = 0; i<=haystack.length()-needle.length()-1;i++){
            int index = 0;

            if(haystack.charAt(i) == needle.charAt(0)){
                for(int j = 0;j<needle.length();j++){
                    if(haystack.charAt(i+j) == needle.charAt(j)){
                        index++;
                    }
                }
            }
            if(index == needle.length()){
                return i;
            }
        }
        return -1;
    }
}
