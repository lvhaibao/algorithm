package slidingwindow;

import java.util.ArrayList;

/**
 * @author lvhaibao
 * @description https://blog.csdn.net/zy450271923/article/details/105324779
 * @date 2021/7/5 21:53
 */
public class test1 {

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> d = FindContinuousSequence(9);

        System.out.println("dd");


    }

    // 解题思路
    // 如果窗口内的数值之和等于sum，则将该窗口内的数添加到list，并将left++
    // 如果窗口内数值之和小于sum，将right++
    // 如果窗口内的数值之和大于sum，将left++
    // 由于窗口的大小最小为2，所以循环结束条件可以设置为left<right
    public static ArrayList<ArrayList<Integer>> FindContinuousSequence(int sum) {

        ArrayList<ArrayList<Integer>> arrayLists = new ArrayList<>();

        int left = 1;
        int right= 2;

        while (left<right){
            int res = 0;
            for(int i = left;i<=right;i++){
                res += i;
            }
            if(res==sum){
                ArrayList<Integer> arrayList = new ArrayList<>();
                for(int i = left;i<=right;i++){
                    arrayList.add(i);
                }
                arrayLists.add(arrayList);
                left++;
            }
            else if(res<sum){
                right++;
            }
            else {
                left++;
            }
        }
        return arrayLists;
    }
}
