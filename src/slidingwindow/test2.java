package slidingwindow;

/**
 * @author lvhaibao
 * @description
 * @date 2021/7/5 22:19
 */
public class test2 {

    public static void main(String[] args) {

//
//        输入: nums = [10,5,2,6], k = 100
//        输出: 8
//        解释: 8个乘积小于100的子数组分别为: [10], [5], [2], [6], [10,5], [5,2], [2,6], [5,2,6]。
//        需要注意的是 [10,5,2] 并不是乘积小于100的子数组。

        int[] a = {10,5,2,6};
        int k = 100;
        int c = numSubarrayProductLessThanK(a,k);

        // [10],[5],[2], [6]




    }

    public static  int numSubarrayProductLessThanK(int[] nums, int k) {
        if(nums.length==0 || k<=1) {
            return 0;
        }
        int val = 1;
        int count = 0;
        int left = 0;
        for (int i = 0; i < nums.length; i++) {
            val = val*nums[i];
            while(val >= k) {
                val = val/nums[left++];
            }
            count = count + i-left+1;
        }
        return count;
    }
}
