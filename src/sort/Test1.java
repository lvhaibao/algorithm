package sort;

/**
 * @author lvhaibao
 * @description 冒泡排序
 * @date 2021/7/7 22:45
 */
public class Test1 {

    public static void main(String[] args) {
        int[] a= {8,5,3,4};

        for(int i = 0;i<a.length;i++){
            for(int j=1;j<a.length-i;j++){
                if(a[j-1] > a[j]){
                    int tmp = a[j-1];
                    a[j-1] = a[j];
                    a[j] = tmp;
                }
            }
        }

        System.out.println(a.toString());
    }
}
